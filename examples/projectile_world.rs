extern crate ray_tracer_challenge;

use ray_tracer_challenge::{point, vector, Canvas, Color, Tuple};
use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
struct Projectile {
    position: Tuple,
    velocity: Tuple,
}
struct World {
    gravity: Tuple,
    wind: Tuple,
}

fn tick(world: &World, projectile: Projectile) -> Projectile {
    let position = projectile.position + projectile.velocity;
    let velocity = projectile.velocity + world.gravity + world.wind;
    return Projectile { position, velocity };
}

fn main() {
    let mut canvas = Canvas::new(900, 550);

    let initial_projectile = Projectile {
        position: point(0.0, 1.0, 0.0),
        velocity: vector(1.0, 1.8, 0.0).normalize() * 11.25,
    };
    let world = World {
        gravity: vector(0.0, -0.1, 0.0),
        wind: vector(-0.01, 0.0, 0.0),
    };

    let mut p = initial_projectile;
    let mut i = 0;
    println!("Canvas: {}x{}", canvas.width, canvas.height);
    while p.position.y > 0.0 {
        p = tick(&world, p);
        let h = canvas.height;
        println!("Projectile: {} {}", p.position.x, p.position.y);
        if p.position.y > 0.0 {
            canvas.write_pixel(
                p.position.x as usize,
                h - p.position.y as usize,
                Color::new(1.0, 0.0, 0.0),
            );
        }
        i += 1;
    }
    let ppm: String = canvas.to_ppm();
    let mut file = File::create("trajectory.ppm").unwrap();
    file.write(ppm.as_bytes()).unwrap();
    println!("Projectile position: {:?}, ticks: {}", p.position, i);
}
