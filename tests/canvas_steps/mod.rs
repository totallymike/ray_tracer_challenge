use ray_tracer_challenge::{Canvas, Color};
use World;

use cucumber_rust::HashableRegex;

steps!(World => {
    given regex r"^([[:alnum:]]+) ← ((?:canvas)\(.+\))$" (String, Canvas) |world, var, canvas, _step| {
        world.canvases.insert(var, canvas);
    };

    when regex r"^write_pixel\(([[:alnum:]]+), (\d+), (\d+), ([[:alnum:]]+)\)" (String, usize, usize, String) |world, canvas_var, x, y, color_var, _step| {
        let canvas = world.canvases.get_mut(&canvas_var).unwrap();
        let color = world.colors[&color_var];

        canvas.write_pixel(x, y, color);
    };

    when regex r"^every pixel of ([[:alnum:]]) is set to (color\(.+\))$" (String, Color::<f32>) |world, canvas_var, color, _step| {
        let canvas = world.canvases.get_mut(&canvas_var).unwrap();
        for y in 0..canvas.height {
            for x in 0..canvas.width {
                canvas.write_pixel(x, y, color);
            }
        }
    };

    when regex r"^([[:alnum:]]+) ← canvas_to_ppm\(([[:alnum:]]+)\)" (String, String) |world, ppm_var, canvas_var, _step| {
        let canvas = world.canvases.remove(&canvas_var).unwrap();
        world.ppms.insert(ppm_var, canvas.to_ppm());
    };

    then regex r"^pixel_at\(([[:alnum:]]+), (\d+), (\d+)\) = ([[:alnum:]]+)" (String, usize, usize, String) |world, canvas_var, x, y, color_var, _step| {
        let canvas = &world.canvases[&canvas_var];
        let color = &world.colors[&color_var];

        assert_eq!(canvas.pixel_at(x, y), color);
    };

    then regex r"^([[:alnum:]]+)\.(height|width) = (\d+)$" (String, String, usize) |world, var, field, num, _step| {
        let canvas = &world.canvases[&var];
        let value = match field.as_str() {
            "height" => canvas.height,
            "width" => canvas.width,
            _ => panic!("Wrong field"),
        };
        assert_eq!(value, num);
    };

    then regex r"\Aevery pixel of ([[:alnum:]]+) is ((?:color)\(.+\))\z" (String, Color::<f32>) |world, var, color, _step| {
        let canvas = &world.canvases[&var];
        assert!(canvas.all(|px| px == &color));
    };

    then regex r"^lines (\d+)-(\d+) of ([[:alnum:]]+) are" (usize, usize, String) |world, line_start, line_end, ppm_var, step| {
        let ppm: &str = &world.ppms[&ppm_var];
        let lines: Vec<&str> = ppm.lines().skip(line_start - 1).take(line_end + 1 - line_start).collect();
        let lines: String = lines.join("\n");
        let expected = step.docstring().unwrap();
        assert_eq!(&lines, expected);
    };

    then regex r"^the last character of ([[:alnum:]]+) is a newline" (String) |world, ppm_var, _step| {
        let ppm = world.ppms.get_mut(&ppm_var).unwrap();
        let last_char = ppm.pop().unwrap();
        ppm.push(last_char);
        assert_eq!(last_char, '\n');
    };
});
