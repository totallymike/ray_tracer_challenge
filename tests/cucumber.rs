#[macro_use]
extern crate cucumber_rust;
extern crate ray_tracer_challenge;

use ray_tracer_challenge::{Canvas, Color, Tuple};
use std::collections::HashMap;

mod canvas_steps;

#[derive(Default)]
pub struct World {
    canvases: HashMap<String, Canvas>,
    tuples: HashMap<String, Tuple>,
    colors: HashMap<String, Color<f32>>,
    ppms: HashMap<String, String>,
}

impl cucumber_rust::World for World {}
mod test_steps {
    use cucumber_rust::HashableRegex;
    use ray_tracer_challenge::{floats_are_equal, Color, Tuple};
    steps!(::World => {
        given regex r"^([[:alnum:]]+) ← (color\(.+\))$" (String, Color::<f32>) |world, var, color, _step| {
            world.colors.insert(var, color);
        };

        given regex r"^([[:alnum:]]+) ← ((?:tuple|point|vector)\(.+\))$" (String, Tuple) |world, var, tuple, _step| {
            world.tuples.insert(var, tuple);
        };

        when regex r"^([[:alnum:]]+) ← normalize\(([[:alnum:]])\)$" (String, String) |world, normal_var, var, _step| {
            let tuple = world.tuples[&var];
            world.tuples.insert(normal_var, tuple.normalize());
        };

        then regex r"^([[:alnum:]]+) = (\w+\(.+\))$" (String, Tuple) |world, var, other, _step| {
            assert_eq!(world.tuples[&var], other);
        };

        then regex r"^([[:alnum:]]+) ([+-]) ([[:alnum:]]+) = ((?:tuple|point|vector)\(.+\))$" (String, char, String, Tuple) |world, var1, op, var2, expected, _step| {
            let tuple1 = world.tuples[&var1];
            let tuple2 = world.tuples[&var2];
            let result = match op {
                '+' => tuple1 + tuple2,
                '-' => tuple1 - tuple2,
                _ => unimplemented!("Operation not implemented: {}", op),
            };
            assert_eq!(result, expected);
        };

        then regex r"^([[:alnum:]]+) ([+-]) ([[:alnum:]]+) = (color\(.+\))$" (String, char, String, Color::<f32>) |world, var1, op, var2, expected, _step| {
            let color1 = world.colors[&var1];
            let color2 = world.colors[&var2];
            let result = match op {
                '+' => color1 + color2,
                '-' => color1 - color2,
                _ => unimplemented!("Operation not implemented: {}", op),
            };
            assert_eq!(result, expected);
        };

        then regex r"^-([[:alnum:]]+) = (\w+\(.+\))$" (String, Tuple) |world, var1, diff, _step| {
            assert_eq!(-world.tuples[&var1], diff);
        };

        then regex r"^([[:alnum:]]+) ([\*/]) (\d\.?\d*) = ((?:tuple|point|vector)\(.+\))$" (String, char, f32, Tuple) |world, var, op, scalar, expected, _step| {
            let result = match op {
                '*' => world.tuples[&var] * scalar,
                '/' => world.tuples[&var] / scalar,
                _ => unimplemented!("Operation not implemented: {}", op),
            };
            assert_eq!(result, expected);
        };

        then regex r"^([[:alnum:]]+) \* (\w[[:alnum:]]) = (color\(.+\))$" (String, String, Color::<f32>) |world, var1, var2, expected, _step| {
            let color1 = world.colors[&var1];
            let color2 = world.colors[&var2];
            assert_eq!(color1 * color2, expected);
        };

        then regex r"^([[:alnum:]]+) \* (\d\.?\d*) = (color\(.+\))$" (String, f32, Color::<f32>) |world, var, scalar, expected, _step| {
            assert_eq!(world.colors[&var] * scalar, expected);
        };

        then regex r"^([[:alnum:]]+) dot ([[:alnum:]]+) = (.+)$" (String, String, f32) |world, var1, var2, result, _step| {
            let tuple1 = world.tuples[&var1];
            let tuple2 = world.tuples[&var2];
            assert_eq!(tuple1.dot(&tuple2), result);
        };

        then regex r"^([[:alnum:]]+) cross ([[:alnum:]]+) = (.+)$" (String, String, Tuple) |world, var1, var2, result, _step| {
            let tuple1 = world.tuples[&var1];
            let tuple2 = world.tuples[&var2];
            assert_eq!(tuple1.cross(&tuple2), result);
        };

        then regex r"^magnitude\(([[:alnum:]]+)\) = (\d\.?\d*)$" (String, f32) |world, var, magnitude, _step| {
            let tuple_magnitude = world.tuples[&var].magnitude();
            assert!(floats_are_equal(tuple_magnitude, magnitude));
        };

        then regex r"^magnitude\(([[:alnum:]]+)\) = √(\d\.?\d*)$" (String, f32) |world, var, magnitude, _step| {
            let sqrt = magnitude.sqrt();
            assert_eq!(world.tuples[&var].magnitude(), sqrt);
        };

        then regex r"^normalize\(([[:alnum:]]+)\) = (.+)$" (String, Tuple) |world, var, vector_normal, _step| {
            assert_eq!(world.tuples[&var].normalize(), vector_normal);
        };

        then regex r"^([[:alnum:]]+)\.([wxyz]) = ([\d\.-]+)$" (String, String, f32) |world, var, field, num, _step| {
            let tuple = world.tuples[&var];
            let value = match field.as_str() {
                "x" => tuple.x,
                "y" => tuple.y,
                "z" => tuple.z,
                "w" => tuple.w,
                _ => panic!("Wrong field"),
            };
            assert_eq!(value, num);
        };

        then regex r"^([[:alnum:]]+)\.(red|green|blue) = ([\d\.-]+)$" (String, String, f32) |world, var, field, num, _step| {
            let color = world.colors[&var];
            let value = match field.as_str() {
                "red" => color.red,
                "green" => color.green,
                "blue" => color.blue,
                _ => panic!("Wrong field"),
            };
            assert_eq!(value, num);
        };

        then regex r"^([[:alnum:]]+) is a (point|vector)$" (String, String) |world, var, tuple_type, _step| {
            let tuple = world.tuples[&var];
            let result = match tuple_type.as_str() {
                "point" => tuple.is_point(),
                "vector" => tuple.is_vector(),
                _ => unimplemented!("don't know that type")
            };
            assert!(result, "{:?} should be a {}", tuple, tuple_type);
        };

        then regex r"^([[:alnum:]]+) is not a (point|vector)$" (String, String) |world, var, tuple_type, _step| {
            let tuple = world.tuples[&var];
            let result = match tuple_type.as_str() {
                "point" => tuple.is_point(),
                "vector" => tuple.is_vector(),
                _ => unimplemented!("don't know that type")
            };
            assert!(!result, "{:?} should not be a {}", tuple, tuple_type);
        };
    });
}

cucumber! {
    features: "./features",
    world: ::World,
    steps: &[
        test_steps::steps,
        canvas_steps::steps,
    ]
}
