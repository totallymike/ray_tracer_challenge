use regex::Regex;
use std::{
    error, fmt,
    ops::{Add, Mul, Sub},
    str::FromStr,
};

use floats_are_equal;

#[derive(Add, Clone, Copy, Debug, Sub)]
pub struct Color<S>
where
    S: Add + Sub,
{
    pub red: S,
    pub green: S,
    pub blue: S,
}

impl<S> Color<S>
where
    S: Add + Sub,
{
    pub fn new(red: S, green: S, blue: S) -> Color<S> {
        Color { red, green, blue }
    }
}

impl PartialEq for Color<f32> {
    fn eq(&self, other: &Color<f32>) -> bool {
        floats_are_equal(self.red, other.red)
            && floats_are_equal(self.green, other.green)
            && floats_are_equal(self.blue, other.blue)
    }
}

impl fmt::Display for Color<f32> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "({red} {green} {blue}",
            red = self.red,
            green = self.green,
            blue = self.blue
        )
    }
}

impl Mul<f32> for Color<f32> {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self {
        Color::new(self.red * rhs, self.green * rhs, self.blue * rhs)
    }
}

impl<S> Mul<Color<S>> for Color<S>
where
    S: Add + Mul + Sub,
    <S as Mul>::Output: Add + Mul + Sub,
{
    type Output = Color<<S as Mul>::Output>;

    fn mul(self, rhs: Color<S>) -> Color<<S as Mul>::Output> {
        Color::new(
            self.red * rhs.red,
            self.green * rhs.green,
            self.blue * rhs.blue,
        )
    }
}

#[derive(Debug)]
pub enum ParseColorError<T>
where
    T: FromStr + fmt::Debug,
    <T as FromStr>::Err: fmt::Debug + fmt::Display,
{
    UnableToParse(String),
    ParseFieldError(<T as FromStr>::Err),
}

impl<S> fmt::Display for ParseColorError<S>
where
    S: FromStr + fmt::Debug,
    <S as FromStr>::Err: fmt::Debug + fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParseColorError::UnableToParse(s) => write!(f, "Unable to parse: {}", s),
            ParseColorError::ParseFieldError(err) => write!(f, "Unable to parse field: {}", err),
        }
    }
}

impl<S> error::Error for ParseColorError<S>
where
    S: FromStr + fmt::Debug,
    <S as FromStr>::Err: fmt::Debug + fmt::Display,
{}

lazy_static! {
    static ref COLOR_PARSER: Regex = Regex::new(r"color\((.+)\)").unwrap();
}

impl<S> FromStr for Color<S>
where
    S: Add + Sub + FromStr + fmt::Debug,
    <S as FromStr>::Err: fmt::Debug + fmt::Display,
{
    type Err = ParseColorError<S>;

    fn from_str(s: &str) -> Result<Color<S>, Self::Err> {
        let caps = COLOR_PARSER
            .captures(s)
            .ok_or(ParseColorError::UnableToParse(s.to_owned()))?;
        let values: Vec<&str> = caps.get(1).unwrap().as_str().split(", ").collect();
        let red: S = values[0]
            .parse()
            .or_else(|e| Err(ParseColorError::ParseFieldError(e)))?;
        let green: S = values[1]
            .parse()
            .or_else(|e| Err(ParseColorError::ParseFieldError(e)))?;
        let blue: S = values[2]
            .parse()
            .or_else(|e| Err(ParseColorError::ParseFieldError(e)))?;
        Ok(Color::new(red, green, blue))
    }
}
