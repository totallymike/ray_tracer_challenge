use regex::Regex;
use std::default::Default;
use std::error;
use std::fmt;
use std::ops::{Div, Mul};
use std::str::FromStr;

use floats_are_equal;

#[derive(Add, Clone, Copy, Debug, Sub, Neg)]
pub struct Tuple {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w: f32,
}

pub fn tuple(x: f32, y: f32, z: f32, w: f32) -> Tuple {
    Tuple::new(x, y, z, w)
}

pub fn point(x: f32, y: f32, z: f32) -> Tuple {
    Tuple { x, y, z, w: 1.0 }
}

pub fn vector(x: f32, y: f32, z: f32) -> Tuple {
    Tuple { x, y, z, w: 0.0 }
}

impl Default for Tuple {
    fn default() -> Tuple {
        Tuple::new(0.0, 0.0, 0.0, 0.0)
    }
}

impl Tuple {
    pub fn new(x: f32, y: f32, z: f32, w: f32) -> Tuple {
        Tuple { x, y, z, w }
    }

    pub fn magnitude(&self) -> f32 {
        (self.x.powf(2.0) + self.y.powf(2.0) + self.z.powf(2.0) + self.w.powf(2.0)).sqrt()
    }

    pub fn normalize(&self) -> Tuple {
        let magnitude = self.magnitude();
        self / magnitude
    }

    pub fn is_point(&self) -> bool {
        floats_are_equal(self.w, 1.0)
    }

    pub fn is_vector(&self) -> bool {
        floats_are_equal(self.w, 0.0)
    }

    pub fn dot(&self, other: &Tuple) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
    }

    pub fn cross(&self, other: &Tuple) -> Tuple {
        vector(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x,
        )
    }
}

impl PartialEq for Tuple {
    fn eq(&self, other: &Tuple) -> bool {
        floats_are_equal(self.x, other.x)
            && floats_are_equal(self.y, other.y)
            && floats_are_equal(self.z, other.z)
            && floats_are_equal(self.w, other.w)
    }
}

impl Mul<f32> for Tuple {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self {
        Tuple::new(self.x * rhs, self.y * rhs, self.z * rhs, self.w * rhs)
    }
}

impl Div<f32> for Tuple {
    type Output = Self;

    fn div(self, rhs: f32) -> Self {
        Tuple::new(self.x / rhs, self.y / rhs, self.z / rhs, self.w / rhs)
    }
}

impl<'a> Div<f32> for &'a Tuple {
    type Output = Tuple;

    fn div(self, rhs: f32) -> Tuple {
        Tuple::new(self.x / rhs, self.y / rhs, self.z / rhs, self.w / rhs)
    }
}

lazy_static! {
    static ref TUPLE_PARSER: Regex =
        Regex::new(r"(?P<type>point|tuple|vector)\((?P<coords>.+)\)").unwrap();
}

impl FromStr for Tuple {
    type Err = TupleParseError;

    fn from_str(s: &str) -> Result<Self, TupleParseError> {
        let caps = TUPLE_PARSER
            .captures(s)
            .ok_or(TupleParseError::UnableToParse(s.to_owned()))?;
        let tuple_type: TupleType = caps
            .name("type")
            .map(|m| match m.as_str() {
                "tuple" => Ok(TupleType::Tuple),
                "point" => Ok(TupleType::Point),
                "vector" => Ok(TupleType::Vector),
                x => Err(TupleParseError::InvalidTupleType(x.to_owned())),
            }).unwrap()?;

        let n_splits = match tuple_type {
            TupleType::Tuple => 4,
            _ => 3,
        };

        let coords: &str = caps.name("coords").map(|m| m.as_str()).unwrap();

        let coords: Vec<&str> = coords.splitn(n_splits, ", ").collect();

        let x: f32 = {
            let val = &coords[0];
            val.parse::<f32>()
                .or_else(|e| Err(TupleParseError::InvalidCoordinate(e)))?
        };
        let y: f32 = {
            let val = &coords[1];
            val.parse::<f32>()
                .or_else(|e| Err(TupleParseError::InvalidCoordinate(e)))?
        };
        let z: f32 = {
            let val = &coords[2];
            val.parse::<f32>()
                .or_else(|e| Err(TupleParseError::InvalidCoordinate(e)))?
        };

        match tuple_type {
            TupleType::Tuple => {
                let w: f32 = {
                    let val = &coords[3];
                    val.parse::<f32>()
                        .or_else(|e| Err(TupleParseError::InvalidCoordinate(e)))?
                };
                Ok(tuple(x, y, z, w))
            }
            TupleType::Point => Ok(point(x, y, z)),
            TupleType::Vector => Ok(vector(x, y, z)),
        }
    }
}

#[derive(Debug)]
pub enum TupleParseError {
    UnableToParse(String),
    InvalidTupleType(String),
    InvalidCoordinate(::std::num::ParseFloatError),
}

impl fmt::Display for TupleParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TupleParseError::UnableToParse(value) => write!(f, "Unable to parse {}", value),
            TupleParseError::InvalidTupleType(type_name) => {
                write!(f, "Invalid tuple type: {}", type_name)
            }
            TupleParseError::InvalidCoordinate(err) => write!(f, "Invalid Coordinate {}", err),
        }
    }
}

impl error::Error for TupleParseError {}
enum TupleType {
    Tuple,
    Point,
    Vector,
}
