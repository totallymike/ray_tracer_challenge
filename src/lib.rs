#[macro_use]
extern crate derive_more;
#[macro_use]
extern crate lazy_static;
extern crate regex;

pub mod canvas;
pub mod color;
pub mod tuple;
pub use canvas::Canvas;
pub use color::Color;
pub use tuple::{point, tuple, vector, Tuple};

pub const FLOAT_EPSILON: f32 = 0.00001;

pub fn floats_are_equal(a: f32, b: f32) -> bool {
    (a - b).abs() < FLOAT_EPSILON
}
