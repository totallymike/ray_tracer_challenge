use regex::Regex;
use std::{
  error,
  fmt::{self, Write},
  num::ParseIntError,
  str::FromStr,
};
use Color;

#[derive(Clone, Debug)]
pub struct Canvas {
  pub height: usize,
  pub width: usize,
  pixels: Vec<Color<f32>>,
}

impl Canvas {
  pub fn new(width: usize, height: usize) -> Canvas {
    let mut pixels = Vec::with_capacity(width * height);
    for _ in 0..width * height {
      pixels.push(Color::new(0.0, 0.0, 0.0));
    }
    Canvas {
      height,
      width,
      pixels,
    }
  }

  pub fn pixel_at(&self, x: usize, y: usize) -> &Color<f32> {
    &self.pixels[self.width * y + x]
  }

  pub fn write_pixel(&mut self, x: usize, y: usize, color: Color<f32>) {
    self.pixels[self.width * y + x] = color;
  }

  pub fn to_ppm(self) -> String {
    let mut buffer = String::new();
    writeln!(buffer, "P3");
    writeln!(buffer, "{} {}", self.width, self.height);
    writeln!(buffer, "255");
    let mut line_buffer = String::with_capacity(70);
    for line in self.pixels.chunks(self.width) {
      for pixel in line {
        let colors = [pixel.red, pixel.green, pixel.blue];
        for color in &colors {
          let mut color_value: f32 = 255u8 as f32 * color;
          if color_value > 255.0 {
            color_value = 255.0;
          } else if color_value < 0.0 {
            color_value = 0.0;
          }
          let color_str = format!("{color:.0}", color = color_value);
          let line_buffer_len = line_buffer.len();
          let extra_space = match line_buffer_len {
            0 => color_str.len(),
            _ => color_str.len() + 1,
          };
          if line_buffer_len + extra_space > 70 {
            line_buffer.push('\n');
            buffer.push_str(&line_buffer);
            line_buffer.clear();
            line_buffer.push_str(&color_str);
          } else {
            if line_buffer.len() > 0 {
              line_buffer.push(' ');
            }
            line_buffer.push_str(&color_str);
          }
        }
      }
      buffer.push_str(&line_buffer);
      line_buffer.clear();
      buffer.push('\n');
    }
    buffer
  }

  pub fn all<F>(&self, f: F) -> bool
  where
    F: FnMut(&Color<f32>) -> bool,
  {
    self.pixels.iter().all(f)
  }
}

#[derive(Debug)]
pub enum ParseCanvasError {
  UnableToParse(String),
  ParseFieldError(ParseIntError),
}

impl error::Error for ParseCanvasError {}
impl fmt::Display for ParseCanvasError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      ParseCanvasError::UnableToParse(s) => write!(f, "Unable to parse: {}", s),
      ParseCanvasError::ParseFieldError(err) => write!(f, "Unable to parse field: {}", err),
    }
  }
}

lazy_static! {
  static ref CANVAS_PARSER: Regex = Regex::new(r"canvas\((.+)\)").unwrap();
}

impl FromStr for Canvas {
  type Err = ParseCanvasError;

  fn from_str(s: &str) -> Result<Canvas, Self::Err> {
    let caps = CANVAS_PARSER
      .captures(s)
      .ok_or(ParseCanvasError::UnableToParse(s.to_owned()))?;
    let values: Vec<&str> = caps.get(1).unwrap().as_str().split(", ").collect();
    let width: usize = values[0]
      .parse()
      .or_else(|e| Err(ParseCanvasError::ParseFieldError(e)))?;
    let height: usize = values[1]
      .parse()
      .or_else(|e| Err(ParseCanvasError::ParseFieldError(e)))?;
    Ok(Canvas::new(width, height))
  }
}
